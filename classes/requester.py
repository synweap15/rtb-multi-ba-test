from random import randint
import socket
from threading import Thread


class Requester():

    _iterations = None
    _tuple_length = None
    _sockets = None

    _in_ip = "127.0.0.1"
    _in_port = 5056
    _out_ip = "127.0.0.1"
    _out_port = 5055

    def __init__(self, iterations, length):
        """
        generator list liczb do obliczenia GCD
        :param iterations: liczba iteracji (losowan liczb do setu)
        :param length: ilosc liczb w secie
        """
        self._iterations = iterations
        self._tuple_length = length
        self._sockets = {
            "out": socket.socket(socket.AF_INET, socket.SOCK_DGRAM),
            "in": socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        }

    def generate_tuple(self):
        return [str(randint(10, 200)) for _ in range(self._tuple_length)]

    def _run_thread(self):
        print("Requester: running")
        while self._iterations > 0:
            self.send("|".join(self.generate_tuple()))
            self._iterations -= 1
        self.send("END")

    def run(self):
        requester_thread = Thread(target=self._run_thread)
        requester_thread.start()

    def send(self, data):
        self._sockets["out"].sendto(data.encode("utf-8"), (self._out_ip, self._out_port))
