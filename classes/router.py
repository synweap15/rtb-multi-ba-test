import random
import socket
from threading import Thread
import time
from classes.biddingagent import BiddingAgent


class Router():
    _bidding_agents = None
    _sockets = None

    _accumulated_tensor = None

    _in_ip = "127.0.0.1"
    _in_port = 5055
    _out_ip = "127.0.0.1"
    _out_port = 5056

    def __init__(self, agents_number):
        """
        Router przekazuje bezposrednio sety do obliczenia do agentow
        :param agents_number: ilosc BA do uruchomienia
        """
        self._accumulated_tensor = set([])
        self._sockets = {
            "out": socket.socket(socket.AF_INET, socket.SOCK_DGRAM),
            "in": socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        }

        self._bidding_agents = []
        for x in range(agents_number):
            self._bidding_agents.append(BiddingAgent(x, set([])))

        reload_thread = Thread(target=self._reload_thread)
        reload_thread.start()

    def _reload_thread(self):
        """
        Losowe wymuszanie synchronizacji

        """
        while True:
            time.sleep(0.05)
            tensor = random.choice(self._bidding_agents).synchronize(self._accumulated_tensor)
            self._accumulated_tensor.update(tensor)


    def _listen_thread(self):
        print("Router: listening")
        self._sockets["in"].bind((self._in_ip, self._in_port))
        while True:
            raw, address = self._sockets["in"].recvfrom(64)
            try:
                decoded = raw.decode("utf-8")
                if decoded == "END":
                    break
                numbers_array = decoded.split("|")

                # zapewnienie ze wybrany agent jest aktywny
                agent = random.choice([x for x in self._bidding_agents if x.active])
                if not agent:
                    print("Zabraklo agentow!")
                    raise Exception
                agent.calculate(numbers_array)
            except Exception as e:
                print(str(e))

    def listen(self):
        router_thread = Thread(target=self._listen_thread)
        router_thread.start()
