from functools import reduce
from fractions import gcd
from threading import Lock, Thread


class BiddingAgent():
    number = None
    active = True
    _current_tensor = None
    _last_tensor = None
    _lock = None

    def __init__(self, number, tensor):
        self.number = number
        self._current_tensor = tensor
        self._last_tensor = tensor
        self._lock = Lock()

    def synchronize(self, new_tensor):
        """
        Zwrocenie aktualnego tensora oraz zapis nowego
        Nie trzeba najpierw go zwracac, poniewaz moze byc obliczony w agencie (kwestia kosztu?)
        :param new_tensor: aktualny tensor pozyskany z bazy
        :return: 'nagromadzony' tensor
        """
        print("Synchronizacja agenta {number}".format(number=self.number))

        self._lock.acquire()
        self.active = False

        temp_old_tensor = self._current_tensor.copy()
        self._current_tensor.update(new_tensor)
        self._last_tensor.update(new_tensor)

        self.active = True
        self._lock.release()

        return temp_old_tensor

    def _calculate_thread(self, array):
        """
        obliczenie GCD i dodanie wyniku do setu wynikow
        :param array: lista liczb do GCD
        """
        self._lock.acquire()
        self._current_tensor.add(reduce(gcd, [int(x) for x in array]))
        print("{number}: {set}".format(number=self.number, set=self._current_tensor))
        self._lock.release()

    def calculate(self, array):
        calculate_thread = Thread(target=self._calculate_thread, args=(array,))
        calculate_thread.start()

