Synchronizacja wiedzy BA - propozycja
=====================================

Kod zawiera
-----------

- bardzo uproszczona struktura dla routera, BA, requestera
- obliczanie GCD zestawu liczb zadanego przez requeste
- komunikacja requester -> router - UDP po localhost
- losowanie asynchroniczne wyłączanie agentów z oblicze do synchronizacji

Opis
----

Requester okresowo generuje zestaw (set) liczb, które przekazuje do routera. Router przydziela zestaw liczb BA losowo.
BA z setem oblicza GCD dla listy liczb. Wynik zostaje umieszczony w lokalnym secie wyników GCD.

Periodycznie losowy BA zostaje wywołany do synchronizacji - lokalny set wyników GCD zostaje zsynchronizowany z setem
wszystkich GCD przechowywanym w routerze. Do BA przekazywany jest nowy, uaktualniony set GCD. Po tej operacji BA wznawia
działanie.

Zestaw testowy może zostać rozbudowany o poprawną komunikację pomiędzy modułami oraz bardziej realną logikę. Ma na celu
jedynie zaprezentowanie koncepcji synchronizacji centralnej wiedzy z wiedzą poszczególnych BA.


Uruchomienie
------------

python tests.py

Niektóre parametry podlegają modyfikacji z poziomu tests.py.