from classes.requester import Requester
from classes.router import Router

router = Router(4)
requester = Requester(40000, 2)

router.listen()
requester.run()